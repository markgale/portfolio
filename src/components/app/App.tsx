import React from 'react';
import './App.css';
import CoinsSection from '../coins/CoinsSection';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CoinsSection />
        <p>Based on Average of Last 24hrs $USD powered by <a href="https://www.kraken.com/en-gb/features/api">Kraken API</a></p>
      </header>
    </div>
  );
}

export default App;
