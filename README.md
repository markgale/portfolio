# My Portfolio

Basic crypto portfolio checking app (powered by kraken data api and uses localstorage for history)

# Quick Start

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
To launch `npm start`

# TODO
* Move price api to its own service out and of display component
* Unit Testing
* Metadata in markup
* Configuration
  * amounts
  * coin types
* PWA
* Badges
  * coverage
  * license?
  * what are the good ones
* deploy on tags?
* Integration / System testing
