import React from 'react';
import { Sparklines, SparklinesLine } from 'react-sparklines-typescript';

interface CoinProps {
  name: string, 
  image: string, 
  prices: number[], 
  holding: number
 }

export default function Coin(props: CoinProps) {
  return (
    <tr>
      <td>{props.name} <img src={props.image} height={40} width={40} alt={props.name + " icon"}/></td>
      <td>
        <Sparklines data={props.prices} limit={5} svgWidth={150} svgHeight={40}>
          <SparklinesLine color="orange" />
        </Sparklines>
      </td>
      <td>${Number(props.prices.slice(-1)[0]).toFixed(4)}</td>
      <td>${Number(props.holding).toFixed(2)}</td>
    </tr>
  );
}
