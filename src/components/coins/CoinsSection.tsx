import React from 'react';
import useAxios from 'axios-hooks'
import Coin from './Coin';
import * as BitCoinLogo from './images/bitcoin-btc-logo.png'
import * as RippleLogo from './images/xrp-xrp-logo.png'

export default function CoinsSection() {

  const [{ data, loading, error }] = useAxios(
    'https://api.kraken.com/0/public/Ticker?pair=XXRPZUSD,XXBTZUSD'
  )
  const btcAmount: number = 0.01;
  const xrpAmount: number = 400;
  const btcKey: string = 'btc-prices';
  const xrpKey: string = 'xrp-prices';
  // some canned historic data
  const btcHist: number[] = [ 34618.70, 37271.80, 39244.90, 33416.60, 40530.60];
  const xrpHist: number[] = [ 0.8631, 0.8439, 0.8047, 0.7350, 0.7757 ];
  const maxHist: number = 5;

  if (loading) return <p>Loading prices...</p>
  if (error) return <p>Failed to fetch coin prices!</p>


  var btcPrices: number[] = [];
  var btcTotal: number = 0;
  var xrpPrices: number[] = [];
  var xrpTotal: number = 0;
  var total: number = 0;
  var totalDelta: string = '';

  var storedBtc = localStorage.getItem(btcKey);
  if(!storedBtc){
    btcPrices = btcHist;
  } else {
    btcPrices = storedBtc.split(',').map(Number);
  }
  let storedXrp = localStorage.getItem(xrpKey);
  if(!storedXrp){
    xrpPrices = xrpHist;
  } else {
    xrpPrices = storedXrp.split(',').map(Number);
  }

  if(data != null){
      btcPrices.push(Number(data?.result.XXBTZUSD.p[1]));
      btcTotal = btcPrices[btcPrices.length - 1] * btcAmount;

      xrpPrices.push(Number(data?.result.XXRPZUSD.p[1]));
      xrpTotal = xrpPrices[xrpPrices.length - 1] * xrpAmount;

      total = xrpTotal + btcTotal;
      
      var delta: number = (btcPrices[btcPrices.length - 2] * btcAmount) + (xrpPrices[xrpPrices.length - 2] * xrpAmount) - total;
      totalDelta = (delta<0?"$":"+$") + Number(delta).toFixed(2);

      // store latest prices (cap at 10)
      localStorage.setItem(btcKey, btcPrices.slice(-maxHist).toString());
      localStorage.setItem(xrpKey, xrpPrices.slice(-maxHist).toString());
  }


  return (
    <div>
      <h3>Portfolio total ${Number(total).toFixed(2)} ({totalDelta})</h3>
      <table>
        <thead>
          <tr><th>Currency</th><th>History</th><th>Price</th><th>Wallet Value</th></tr>
        </thead>
        <tbody>
          <Coin name="Bitcoin" image={BitCoinLogo} prices={btcPrices} holding={btcTotal} />
          <Coin name="Ripple" image={RippleLogo} prices={xrpPrices} holding={xrpTotal} />
        </tbody>
      </table>
    </div>
  );
}
