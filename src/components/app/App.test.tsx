import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders title of app', () => {
  const { getByText } = render(<App />);
  const title = getByText(/last 24hrs \$usd/i);
  expect(title).toBeInTheDocument();
});
